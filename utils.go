package actions

import (
	"os"
	"time"
)

// FileExists returns true if the file exists and is not a directory.
func FileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

// DirExists returns true if the path exists and is a directory.
func DirExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return info.IsDir()
}

// FormatDuration format duration string.
func FormatDuration(duration time.Duration) string {
	return duration.Round(time.Second).String()
}
