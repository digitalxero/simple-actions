// Package actions contains the high level workflow actions
package actions

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/digitalxero/simple-actions/log"
	"gitlab.com/digitalxero/simple-actions/term"
)

type defaultActionContext struct {
	ctx context.Context
	logger log.Logger
	status *term.Status
	dryRun bool
}

// NewDefaultActionContext generate a default action context with a nil return for Data().
func NewDefaultActionContext(logger log.Logger, dryRun bool) ActionContext {
	return &defaultActionContext{
		ctx: context.Background(),
		logger: logger,
		status: term.StatusForLogger(logger),
		dryRun: dryRun,
	}
}

// WithCancel wrap context.WithCancel
func (ac *defaultActionContext) WithCancel() (ctx ActionContext, cancel context.CancelFunc) {
	var nctx context.Context
	nctx, cancel = context.WithCancel(ac.ctx)
	ac.ctx = nctx
	return ac, cancel
}

// WithDeadline wrap context.WithDeadline
func (ac *defaultActionContext) WithDeadline(d time.Time) (ctx ActionContext, cancel context.CancelFunc) {
	var nctx context.Context
	nctx, cancel = context.WithDeadline(ac.ctx, d)
	ac.ctx = nctx
	return ac, cancel
}

// WithTimeout wrap context.WithTimeout
func (ac *defaultActionContext) WithTimeout(d time.Duration) (ctx ActionContext, cancel context.CancelFunc) {
	var nctx context.Context
	nctx, cancel = context.WithTimeout(ac.ctx, d)
	ac.ctx = nctx
	return ac, cancel
}

// WithValue wrap context.WithValue
func (ac *defaultActionContext) WithValue(key, val interface{}) ActionContext {
	ac.ctx = context.WithValue(ac.ctx, key, val)
	return ac
}

// Logger returns the logger.
func (ac *defaultActionContext) Logger() log.Logger {
	return ac.logger
}

// Status returns the term.Status.
func (ac *defaultActionContext) Status() *term.Status {
	return ac.status
}

// IsDryRun defines if this should be a dry run.
func (ac *defaultActionContext) IsDryRun() bool {
	return ac.dryRun
}

func (ac *defaultActionContext) Deadline() (deadline time.Time, ok bool) {
	return ac.ctx.Deadline()
}

func (ac *defaultActionContext) Done() <-chan struct{} {
	return ac.ctx.Done()
}

func (ac *defaultActionContext) Err() error {
	return ac.ctx.Err()
}

func (ac *defaultActionContext) Value(key interface{}) interface{} {
	return ac.ctx.Value(key)
}


// Data returns nil.
func (ac *defaultActionContext) Data() interface{} {
	return nil
}

type sleepAction struct {
	duration string
	msg      string
}

// NewSleepAction docs.
func NewSleepAction(duration, msg string) Action {
	return &sleepAction{duration: duration, msg: msg}
}

// Execute documentation.
func (a *sleepAction) Execute(ctx ActionContext) (err error) {
	var dur time.Duration
	if dur, err = time.ParseDuration(a.duration); err != nil {
		return err
	}

	ctx.Status().Start(fmt.Sprintf("Sleeping for %s %s", a.duration, a.msg))
	defer func() {
		ctx.Status().End(err == nil)
	}()
	if ctx.IsDryRun() {
		return nil
	}

	time.Sleep(dur)

	return nil
}

type printAction struct {
	msg string
}

// NewPrintAction documentation.
func NewPrintAction(msg string) Action {
	return &printAction{
		msg: msg,
	}
}

// Execute documentation.
func (a *printAction) Execute(ctx ActionContext) error {
	ctx.Status().Start(a.msg)
	ctx.Status().End(true)

	return nil
}

// errorAction docs.
type errorAction struct {
	err error
	msg string
}

// NewErrorAction docs.
func NewErrorAction(err error, msg string) Action {
	return &errorAction{err: err, msg: msg}
}

// Execute documentation.
func (a *errorAction) Execute(ctx ActionContext) (err error) {
	ctx.Status().Start(a.msg)
	ctx.Status().End(a.err == nil)

	return a.err
}
