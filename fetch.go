package actions

import (
	"context"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"strings"

	"github.com/Azure/go-autorest/autorest"
	"github.com/hashicorp/go-azure-helpers/authentication"
	"github.com/tombuildsstuff/giovanni/storage/2019-12-12/blob/blobs"
	"github.com/tombuildsstuff/giovanni/storage/2019-12-12/blob/containers"
)

// AzureENVPrefix docs
// nolint:gochecknoglobals
var AzureENVPrefix = "AZURE"

// fetchFileAction docs.
type fetchFileAction struct {
	filePath, path string
	client         *http.Client
}

// NewFetchFileAction will download a path to a local file. It's efficient because it will
// write as it downloads and not load the whole file into memory.
// accepted scheme are http/https and azure
// the azure schema is a special format `azure://<azure storage account>/<azure container name>/<azure path to blob>
// eg azure://example/my-container/some/file/path
// the http equivalent would be https://example.blob.core.windows.net/my-container/some/file/path
func NewFetchFileAction(filePath, path string) Action {
	return &fetchFileAction{
		filePath: filePath,
		path:     path,
		client:   http.DefaultClient,
	}
}

// NewFetchFileActionWithHTTPClient will download a path to a local file. It's efficient because it will
// write as it downloads and not load the whole file into memory.
// accepted scheme are http/https and azure
// the azure schema is a special format `azure://<azure storage account>/<azure container name>/<azure path to blob>
// eg azure://example/my-container/some/file/path
// the http equivalent would be https://example.blob.core.windows.net/my-container/some/file/path
func NewFetchFileActionWithHTTPClient(filePath, path string, client *http.Client) Action {
	return &fetchFileAction{
		filePath: filePath,
		path:     path,
		client:   client,
	}
}

// Execute documentation.
func (a *fetchFileAction) Execute(ctx ActionContext) (err error) {
	var uri *url.URL
	ctx.Status().Start(fmt.Sprintf("Fetching %s", a.path))
	defer func() {
		ctx.Status().End(err == nil)
	}()
	if ctx.IsDryRun() {
		return nil
	}

	if uri, err = url.Parse(a.path); err != nil {
		return err
	}

	switch uri.Scheme {
	case "http", "https":
		err = a.fetchURL(uri)
		return err
	case "azure":
		err = a.fetchAzureURL(uri)
		return err
	default:
		err = NewCopyPathAction(a.path, a.filePath).Execute(ctx)
		return err
	}
}

func (a *fetchFileAction) fetchURL(uri *url.URL) (err error) {
	var (
		resp *http.Response
		out  *os.File
	)
	// Get the data
	resp, err = a.client.Get(uri.String())
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	// Create the file
	out, err = os.Create(a.filePath)
	if err != nil {
		return err
	}
	defer out.Close()

	// write the body to file
	_, err = io.Copy(out, resp.Body)
	return err
}

func (a *fetchFileAction) fetchAzureURL(uri *url.URL) (err error) {
	var (
		accountName,
		container,
		blobPath string
	)
	accountName = uri.Host
	parts := strings.Split(uri.Path, "/")
	container = parts[1]
	blobPath = strings.Join(parts[2:], "/")

	client, err := buildClient()
	if err != nil {
		return fmt.Errorf("error creating client: %w", err)
	}
	var blob blobs.GetResult
	if blob, err = client.blobs.Get(context.Background(), accountName, container, blobPath, blobs.GetInput{}); err != nil {
		err = fmt.Errorf("error fetching blob: %w", err)
		return err
	}

	if err = ioutil.WriteFile(a.filePath, blob.Contents, 0666); err != nil {
		err = fmt.Errorf("error writing blob: %w", err)
		return err
	}

	return err
}

type azClient struct {
	containers containers.Client
	blobs      blobs.Client
}

func buildClient() (*azClient, error) {
	// we're using github.com/hashicorp/go-azure-helpers since it makes this simpler
	// but you can use an Authorizer from github.com/Azure/go-autorest directly too
	builder := &authentication.Builder{
		ClientID:     os.Getenv(fmt.Sprintf("%s_CLIENT_ID", AzureENVPrefix)),
		TenantID:     os.Getenv(fmt.Sprintf("%s_TENANT_ID", AzureENVPrefix)),
		SubscriptionID:     os.Getenv(fmt.Sprintf("%s_SUBSCRIPTION_ID", AzureENVPrefix)),
		ClientSecret: os.Getenv(fmt.Sprintf("%s_CLIENT_SECRET", AzureENVPrefix)),
		Environment: os.Getenv(fmt.Sprintf("%s_ENVIRONMENT", AzureENVPrefix)),

		// Feature Toggles
		SupportsClientSecretAuth: true,
		SupportsAzureCliToken:    true,
	}

	config, err := builder.Build()
	if err != nil {
		return nil, fmt.Errorf("error building AzureRM azClient: %w", err)
	}

	env, err := authentication.DetermineEnvironment(config.Environment)
	if err != nil {
		return nil, fmt.Errorf("error determining environment: %w", err)
	}

	oauthConfig, err := config.BuildOAuthConfig(env.ActiveDirectoryEndpoint)
	if err != nil {
		return nil, fmt.Errorf("error building OAuthConfig: %w", err)
	}

	// OAuthConfigForTenant returns a pointer, which can be nil.
	if oauthConfig == nil {
		return nil, fmt.Errorf("unable to configure OAuthConfig for tenant %s", config.TenantID)
	}

	// support for HTTP Proxies
	sender := autorest.DecorateSender(&http.Client{
		Transport: &http.Transport{
			Proxy: http.ProxyFromEnvironment,
		},
	})

	storageAuth, err := config.GetAuthorizationToken(sender, oauthConfig, "https://storage.azure.com/")
	if err != nil {
		return nil, fmt.Errorf("error getting auth token: %w", err)
	}

	containersClient := containers.New()
	containersClient.Client.Authorizer = storageAuth
	blobsClient := blobs.New()
	blobsClient.Client.Authorizer = storageAuth

	result := &azClient{
		containers: containersClient,
		blobs:      blobsClient,
	}

	return result, nil
}
