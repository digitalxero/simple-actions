package actions

import (
	"context"
	"time"

	"gitlab.com/digitalxero/simple-actions/log"
	"gitlab.com/digitalxero/simple-actions/term"
)

// ActionContext docs.
type ActionContext interface {
	context.Context

	Logger() log.Logger
	Status() *term.Status
	IsDryRun() bool
	Data() interface{}

	// Builder functions
	WithCancel() (ActionContext, context.CancelFunc)
	WithDeadline(d time.Time) (ActionContext, context.CancelFunc)
	WithTimeout(timeout time.Duration) (ActionContext, context.CancelFunc)
	WithValue(key, val interface{}) ActionContext
}

// Action documentation.
type Action interface {
	Execute(ctx ActionContext) error
}

// Actions list of action items.
type Actions []Action

// Execute executes all action items in the Actions list.
func (a Actions) Execute(ctx ActionContext) (err error) {
	for _, action := range a {
		if err = action.Execute(ctx); err != nil {
			return err
		}
	}

	return nil
}
