

<!--- next entry here -->

## 1.5.5
2021-03-19

### Fixes

- ensure context is not operating on a nil pointer (7715953d421a4d3c9a92ff026f4169e9fa2212f7)
- ensure context is not operating on a nil pointer (eb878b9389f215add45b4f6197d1916cc9fedc14)

## 1.5.4
2021-02-27

### Fixes

- ensure all required env vars are pulled in for the azure client (fd9c8d539f2103b74cb22e86a790df0175cb10e6)

## 1.5.3
2021-02-26

### Fixes

- allow changing the env var prefix for azure (d01bb661743f01f3c9ff1c0f99e46cbf0423a7e9)
- lint issue (52a394c9836b3bba7acf0db73c597d780208c695)

## 1.5.2
2021-02-26

### Fixes

- disable the cli auth if client secret is set (284339c61fecc694d628ad871226420468bd174e)

## 1.5.0
2021-02-25

### Features

- make the ActionContext also be a context.Context (d9e6224176cc331cdd4de5abcaa8fe3481ce9d85)

### Fixes

- lint issues (3a24333884493baecebda69477e5b679f736559e)

## 1.4.0
2021-02-25

### Features

- bring the logger into simple-action instead of requiring the kind logger (42755cd007b357e2b101504e6f89348cbf06f5be)

### Fixes

- lint issues (259d33c44d9c4747031692b378a581b98e37e301)
- lint issues (3b9f4196d1c287bcecee67f671cfc8659ff07ab7)

## 1.3.1
2020-05-26

### Fixes

- deps update (b5ffec29774ee4b6a7e4c411e5a62183bd688737)
- Merge branch 'master' of gitlab.com:digitalxero/simple-actions (0b17a486a3f562babfbf46872f6208f49e3c9256)

## 1.3.0
2020-05-26

### Features

- split the certs, docker, and kubectl actions into their own repo (9817865867c4247a2d69576c72371852860e5000)

## 1.2.4
2020-04-08

### Fixes

- chrome marks all certs with more then 2 years expire time as revoked so allow expire time to be passed in (9041af80bc731df234d54f1bb138e8c26c6a01df)

## 1.2.3
2020-03-17

### Fixes

- allow custom client auth file name prefixes (dc98d29b46bc9e705dd92983d0d5b77f5528dae6)

## 1.2.2
2020-03-17

### Fixes

- ensure client auth certs are able to auth (3fbf4108ca7a061ae14d020ff6789705552206be)

## 1.2.1
2020-03-16

### Fixes

- remove hard coded CA subject (73ccdd7ff8a7951ab98c0e731fc48ba40d61d100)
- allow creation of client auth certs (1df30b271d1689b6bd9d6f7de4f0bf4d3f80682d)

## 1.2.0
2020-03-16

### Features

- add cert related actions, add copy file action (ff85d066a7c57e7134aa157a227f73c58d6eb748)

## 1.1.0
2020-03-06

### Features

- add docker.NewRemoveAction to be able to properly remove a stopped container (3d22a95315888a7d981b719006186892392ca9af)

## 1.0.1
2020-01-04

### Fixes

- create an ActionContext interface to make creating your own actions easier (785b1d53885e77a546ea8371bb501d416d627cf2)
- ensure docker tests pass in CI (3fb40c7bb2f096b1cf7c55f8d6b895a69edae930)
- test why the next version is not bumping properly (ea5f6bbf7b35b7fc983bffd558aac2363fecf368)
- test why the next version is not bumping properly (3b681c55179a08ea159101f97a60e8dd05add962)
- test why the next version is not bumping properly (04bc0919c3dd70faeeedb2e23c5641f6765834b0)
- test why the next version is not bumping properly (886eb8ab063be5f0d2637e7b678ed8b4416109da)

## 1.0.0
2020-01-03

### Features

- initial release of the simple actions interface (29becbe913933d26050890554e05c0b055bd2f09)

### Fixes

- syntax error in .gitlab-ci (7b6c0d77e6cec243e2abb50f9b15d3846d3c3301)
- test the tag and release process (b9e5d33e8f8c1b4dc5fbafdb3b56e60b5b268934)
- define the GL_TOKEN in the project cicd env vars (e3ee1765789a1b85f66e4a8a4adb7ab2cd69ada3)
- commit then tag so the CHANGLOG is correct for the tag (e48a56bb1a1da80241261d08f1df729a0ad31326)
- ensure the lint stage does not run on tags (9fbdf07f36ccdd033f4d999e65405c25a00384df)
