.PHONY: all githooks clean deps test lint out_dir

OUT_DIR ?= build
GOLANGCI_LINT_VERSION ?= v1.21.0
GOARCH  ?= amd64
GOOS    ?= linux
SEMVER  ?= 0.0.0-$(shell whoami)


all: deps test lint build

init: githooks
	curl -sfL https://install.goreleaser.com/github.com/golangci/golangci-lint.sh | sh -s -- -b $(shell go env GOPATH)/bin $(GOLANGCI_LINT_VERSION)

githooks:
	git config core.hooksPath .githooks

out_dir:
	mkdir -p $(OUT_DIR)

test: out_dir
	go test -race ./... -tags 'release netgo osusergo' -v -coverprofile=$(OUT_DIR)/coverage.out
	go tool cover -func=$(OUT_DIR)/coverage.out

#
# lint go code critical checks
#
# Shortcut for lint for developer
lint: out_dir
	$(if $(CI), make ci_lint, make lint_local)

lint_local:
	golangci-lint run --fix -v

# Running lint with additional checks for sonarscanner
# Used by CI
ci_lint:
	golangci-lint run

format:
	go fmt ./...

deps:
	go get -u ./...
	go mod tidy

