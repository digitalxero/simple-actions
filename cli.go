package actions

import (
	"fmt"
	"os/exec"
	"strings"
)

// cliAction documentation.
type cliAction struct {
	msg,
	cmd string
	args []string
}

// NewCLIAction run a cli command as an action.
func NewCLIAction(msg, cmd string, args ...string) Action {
	if msg == "" {
		msg = fmt.Sprintf("Running `%s %s`", cmd, strings.Join(args, " "))
	}
	return &cliAction{
		msg:  msg,
		cmd:  cmd,
		args: args,
	}
}

// Execute documentation.
func (a *cliAction) Execute(ctx ActionContext) (err error) {
	ctx.Status().Start(a.msg)
	defer func() {
		ctx.Status().End(err == nil)
	}()
	if ctx.IsDryRun() {
		return nil
	}

	// nolint:gosec
	err = exec.Command(a.cmd, a.args...).Run()

	return err
}
