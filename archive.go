package actions

import (
	"fmt"
	"os"

	"github.com/mholt/archiver/v3"
)

// archivePathAction documentation.
type archivePathAction struct {
	name  string
	files []string
}

// NewArchivePathAction documentation.
func NewArchivePathAction(name string, files ...string) Action {
	return &archivePathAction{
		name:  name,
		files: files,
	}
}

// Execute documentation.
func (a *archivePathAction) Execute(ctx ActionContext) (err error) {
	ctx.Status().Start(fmt.Sprintf("Creating archive %s from files %v", a.name, a.files))
	defer func() {
		ctx.Status().End(err == nil)
	}()
	if ctx.IsDryRun() {
		return nil
	}

	_ = os.Remove(a.name)
	err = archiver.Archive(a.files, a.name)

	return err
}

// unarchiveAction documentation.
type unarchiveAction struct {
	name, path string
}

// NewUnArchiveAction documentation.
func NewUnArchiveAction(name, path string) Action {
	return &unarchiveAction{
		name: name,
		path: path,
	}
}

// Execute documentation.
func (a *unarchiveAction) Execute(ctx ActionContext) (err error) {
	ctx.Status().Start(fmt.Sprintf("Expanding archive %s => %s", a.name, a.path))
	defer func() {
		ctx.Status().End(err == nil)
	}()
	if ctx.IsDryRun() {
		return nil
	}

	err = archiver.Unarchive(a.name, a.path)

	return err
}