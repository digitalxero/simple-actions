package actions

import (
	"os"
	"testing"

	"gitlab.com/digitalxero/simple-actions/log"
	"gitlab.com/digitalxero/simple-actions/term"
)

func TestBasicAction(t *testing.T) {
	testCtx := NewDefaultActionContext(term.NewLogger(os.Stderr, log.Level(3)), false)
	action := NewPrintAction("testing")
	if err := action.Execute(testCtx); err != nil {
		t.Fatal(err)
	}
}
