module gitlab.com/digitalxero/simple-actions

go 1.13

require (
	github.com/Azure/azure-sdk-for-go v51.3.0+incompatible // indirect
	github.com/Azure/go-autorest/autorest v0.11.18
	github.com/andybalholm/brotli v1.0.1 // indirect
	github.com/drone/envsubst v1.0.2
	github.com/golang/snappy v0.0.3 // indirect
	github.com/hashicorp/errwrap v1.1.0 // indirect
	github.com/hashicorp/go-azure-helpers v0.14.0
	github.com/hashicorp/go-multierror v1.1.0 // indirect
	github.com/klauspost/compress v1.11.8 // indirect
	github.com/klauspost/pgzip v1.2.5 // indirect
	github.com/mattn/go-isatty v0.0.12
	github.com/mholt/archiver/v3 v3.5.0
	github.com/otiai10/copy v1.5.0
	github.com/pierrec/lz4/v4 v4.1.3 // indirect
	github.com/tombuildsstuff/giovanni v0.15.1
	github.com/ulikunitz/xz v0.5.10 // indirect
	golang.org/x/sys v0.0.0-20210225134936-a50acf3fe073 // indirect
	sigs.k8s.io/kind v0.10.0
)
