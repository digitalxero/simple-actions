package actions

import (
	"fmt"
	"os"

	"github.com/drone/envsubst"
	"github.com/otiai10/copy"
)

// createDirectoryAction documentation.
type createDirectoryAction struct {
	path string
}

// NewCreateDirectoryAction documentation.
func NewCreateDirectoryAction(path string) Action {
	path, _ = envsubst.EvalEnv(path)
	path = os.ExpandEnv(path)
	return &createDirectoryAction{
		path: path,
	}
}

// Execute documentation.
func (a *createDirectoryAction) Execute(ctx ActionContext) (err error) {
	ctx.Status().Start(fmt.Sprintf("Creating directory %s", a.path))
	defer func() {
		ctx.Status().End(err == nil)
	}()
	if ctx.IsDryRun() {
		return nil
	}

	if err = os.MkdirAll(a.path, os.ModeDir|os.ModePerm); err != nil {
		return err
	}

	return nil
}

// copyFileAction documentation.
type copyFileAction struct {
	from,
	to string
}

// NewCopyPathAction documentation.
func NewCopyPathAction(from, to string) Action {
	from, _ = envsubst.EvalEnv(from)
	from = os.ExpandEnv(from)
	to, _ = envsubst.EvalEnv(to)
	to = os.ExpandEnv(to)
	return &copyFileAction{
		from: from,
		to:   to,
	}
}

// NewCopyFileAction documentation.
func NewCopyFileAction(from, to string) Action {
	from, _ = envsubst.EvalEnv(from)
	from = os.ExpandEnv(from)
	to, _ = envsubst.EvalEnv(to)
	to = os.ExpandEnv(to)
	return &copyFileAction{
		from: from,
		to:   to,
	}
}

// Execute documentation.
func (a *copyFileAction) Execute(ctx ActionContext) (err error) {
	ctx.Status().Start(fmt.Sprintf("Copying file %s => %s", a.from, a.to))
	defer func() {
		ctx.Status().End(err == nil)
	}()
	if ctx.IsDryRun() {
		return nil
	}

	err = copy.Copy(a.from, a.to, copy.Options{Sync: true})

	return err
}

// renameFileAction documentation.
type renameFileAction struct {
	from,
	to string
}

// NewRenamePathAction documentation.
func NewRenamePathAction(from, to string) Action {
	from, _ = envsubst.EvalEnv(from)
	from = os.ExpandEnv(from)
	to, _ = envsubst.EvalEnv(to)
	to = os.ExpandEnv(to)
	return &renameFileAction{
		from: from,
		to:   to,
	}
}

// Execute documentation.
func (a *renameFileAction) Execute(ctx ActionContext) (err error) {
	ctx.Status().Start(fmt.Sprintf("Renaming file %s => %s", a.from, a.to))
	defer func() {
		ctx.Status().End(err == nil)
	}()
	if ctx.IsDryRun() {
		return nil
	}

	err = os.Rename(a.from, a.to)

	if os.IsNotExist(err) {
		err = nil
	}

	return err
}

// deletePathAction documentation.
type deletePathAction struct {
	path string
}

// NewDeletePathAction documentation.
func NewDeletePathAction(path string) Action {
	path, _ = envsubst.EvalEnv(path)
	path = os.ExpandEnv(path)
	return &deletePathAction{
		path: path,
	}
}

// Execute documentation.
func (a *deletePathAction) Execute(ctx ActionContext) (err error) {
	ctx.Status().Start(fmt.Sprintf("Removing path %s", a.path))
	defer func() {
		ctx.Status().End(err == nil)
	}()
	if ctx.IsDryRun() {
		return nil
	}

	_ = os.RemoveAll(a.path)

	return nil
}